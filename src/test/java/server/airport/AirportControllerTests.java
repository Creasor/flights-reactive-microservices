package server.airport;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.AutoConfigureDataR2dbc;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import server.common.model.Airport;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static server.common.Constants.EndPoint.AIRPORTS;

@WebFluxTest(controllers = {AirportController.class})
@AutoConfigureDataR2dbc
public class AirportControllerTests {

    @Autowired
    WebTestClient webTestClient;

    @MockBean
    private AirportService service;

    @Test
    public void testGetAirports() {
        Airport[] expected = new Airport[]{
                new Airport("ABC", "ABC description"),
                new Airport("DEF", "DEF description"),
                new Airport("HIJ", "HIJ description")
        };
        Flux<Airport> flux = Flux.fromArray(expected);

        when(service.getAirports()).thenReturn(flux);

        List<Airport> result = webTestClient.get()
                .uri("/" + AIRPORTS)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Airport.class)
                .returnResult()
                .getResponseBody();

        verify(service, times(1)).getAirports();

        assertThat(result).isEqualTo(Arrays.asList(expected));
    }
}