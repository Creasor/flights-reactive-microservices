package server.airport;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.r2dbc.dialect.H2Dialect;
import org.springframework.r2dbc.core.DatabaseClient;
import reactor.test.StepVerifier;
import server.common.model.Airport;

import static org.assertj.core.api.Assertions.assertThat;

@DataR2dbcTest(properties = "app.data=")
public class AirportRepositoryTests {

    @Autowired
    private DatabaseClient databaseClient;

    @Autowired
    private AirportRepository repository;

    @Test
    public void testFindAllAirports() {
        Airport airport = new Airport("ABC", "Test Airport");

        R2dbcEntityTemplate template = new R2dbcEntityTemplate(databaseClient, H2Dialect.INSTANCE);

        template.insert(Airport.class)
                .using(airport)
                .then()
                .as(StepVerifier::create)
                .verifyComplete();

        repository.findAll()
                .as(StepVerifier::create)
                .assertNext(actual -> {
                    assertThat(actual.getAirportCode()).isEqualTo(airport.getAirportCode());
                    assertThat(actual.getAirportName()).isEqualTo(airport.getAirportName());
                })
                .verifyComplete();
    }
}