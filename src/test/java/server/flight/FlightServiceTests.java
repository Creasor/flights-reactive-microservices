package server.flight;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.test.StepVerifier;
import server.airline.FlightFactory;
import server.common.model.Airport;
import server.common.model.ExchangeRate;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * These use mocking to isolate and test only the service component.
 */
@AutoConfigureMockMvc
@DataR2dbcTest(properties = {"app.data=", "app.schema="})
@ContextConfiguration(classes = {FlightApplication.class, FlightService.class})
@Timeout(8)
public class FlightServiceTests {
    private MockWebServer mockBackEnd;
    private ObjectMapper objectMapper;
    private FlightService service;
    private List<String> airlineServices;

    @MockBean
    DiscoveryClient discoveryClientMock;

    /**
     * Test using a few airline microservices.
     */
    private final List<String> airlineCodes = List.of("1", "2", "3");

    @BeforeEach
    public void beforeEach() throws IOException {
        mockBackEnd = new MockWebServer();
        mockBackEnd.start();
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        service = new FlightService();
        service.baseUrl = String.format("http://localhost:%d/", mockBackEnd.getPort());
        service.webClient = WebClient.builder().build();

        // Create mock airline microservices that are returned
        // when the service call the discoveryClient.getServices()
        // to enumerate over all known airline microservices.
        airlineServices = airlineCodes
                .stream()
                .map(code -> code + "-airline")
                .collect(Collectors.toList());
        when(discoveryClientMock.getServices()).thenReturn(airlineServices);
        service.discoveryClient = discoveryClientMock;
    }

    @AfterEach
    public void afterEach() throws IOException {
        mockBackEnd.shutdown();
    }

    @Test
    public void testGetAirports() throws Exception {
        List<Airport> expected = new ArrayList<>();
        expected.add(new Airport("ABC", "ABC description"));
        expected.add(new Airport("DEF", "DEF description"));
        expected.add(new Airport("HIJ", "HIJ description"));

        mockBackEnd.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(expected))
                .addHeader("Content-Type", "application/json"));

        StepVerifier.create(service.getAirports())
                .expectNextSequence(expected)
                .verifyComplete();
    }

    @Test
    public void testFindExchangeRate() throws JsonProcessingException {
        ExchangeRate expected = ExchangeRate.of("CAN", "USD", 0.68);

        mockBackEnd.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(expected))
                .addHeader("Content-Type", "application/json"));

        StepVerifier.create(service.getRate(expected.getFromCurrency(), expected.getToCurrency()))
                .expectNext(expected)
                .verifyComplete();
    }

    @Test
    public void testFindExchangeRates() throws JsonProcessingException {
        String toCurrency = "USD";
        List<ExchangeRate> expected = List.of(
                ExchangeRate.of("CAN", toCurrency, 0.68),
                ExchangeRate.of("JPY", toCurrency, 0.0092)
        );

        mockBackEnd.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(expected))
                .addHeader("Content-Type", "application/json"));

        StepVerifier.create(service.getRates("USD"))
                .expectNextSequence(expected)
                .verifyComplete();
    }

    @Test
    public void testFindFlightDates() throws Exception {
        // Expect a single date from each airline.
        List<LocalDate> expected = new ArrayList<>();
        for (int i = 0; i < airlineCodes.size(); i++) {
            LocalDate localDate = LocalDate.parse(String.format("0000-01-%02d", i + 1));
            mockBackEnd.enqueue(new MockResponse()
                    .setBody(objectMapper.writeValueAsString(List.of(localDate)))
                    .addHeader("Content-Type", "application/json"));
            expected.add(localDate);
        }

        String from = "here";
        String to = "there";

        StepVerifier.FirstStep<LocalDate> stepVerifier =
                StepVerifier.create(service.findDepartureDates(from, to));

        for (int i = 0; i < expected.size(); i++) {
            stepVerifier.consumeNextWith(date -> {
                assertThat(date).isIn(expected);
                assertThat(expected.remove(date)).isTrue();
            });
        }

        stepVerifier.verifyComplete();

        // Ensure that all expected flights were received.
        assertThat(expected).isEmpty();
    }

    @Test
    public void testFindFlights() throws Exception {
        // Exchange rate microservice is called first.
        ExchangeRate expectedRates = ExchangeRate.of("CAN", "USD", 0.68);
        mockBackEnd.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(expectedRates))
                .addHeader("Content-Type", "application/json"));

        // Build expected FLIGHT request result lists for
        // each airline and then add them to the mock
        // back-end queue.
        List<Flight> expected = new ArrayList<>();
        for (String airlineCode : airlineCodes) {
            List<Flight> flights = buildExpectedFlights(airlineCode);
            mockBackEnd.enqueue(new MockResponse()
                    .setBody(objectMapper.writeValueAsString(flights))
                    .addHeader("Content-Type", "application/json"));
            expected.addAll(flights);
        }

        // We know that the first flight of the combined list of expected
        // results will return the correct flights from each of the
        // airline services.
        FlightRequest flightRequest =
                FlightFactory.buildRequestFrom(expected.get(0));

        // The service uses a parallel scheduler to forward requests to
        // microservices in an arbitrary order and therefore the resulting
        // Flux of Flight objects will also have an arbitrary order. The
        // StepVerifier only provides a consumeNextSequence which does not
        // support unordered comparisons. Therefore, it's necessary to check
        // each Flight individually by using a loop too add the correct
        // number of consumeWithNext() calls.
        StepVerifier.FirstStep<Flight> stepVerifier =
                StepVerifier.create(service.findFlights(
                        flightRequest.getDepartureAirport(),
                        flightRequest.getDepartureDate(),
                        flightRequest.getArrivalAirport(),
                        flightRequest.getCurrency()));

        for (int i = 0; i < expected.size(); i++) {
            stepVerifier.consumeNextWith(flight -> {
                assertThat(flight).isIn(expected);
                assertThat(expected.remove(flight)).isTrue();
            });
        }

        stepVerifier.verifyComplete();

        // Ensure that all expected flights were received.
        assertThat(expected).isEmpty();
    }

    @Test
    public void testFindBestPrice() throws Exception {
        // Exchange rate microservice is called first.
        ExchangeRate expectedRates = ExchangeRate.of("CAN", "USD", 0.68);
        mockBackEnd.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(expectedRates))
                .addHeader("Content-Type", "application/json"));

        // Build expected FLIGHT request result lists for
        // each airline and then add them to the mock
        // back-end queue.
        List<Flight> expected = new ArrayList<>();
        List<Flight> bestPrice = new ArrayList<>();

        for (String airlineCode : airlineCodes) {
            List<Flight> nextExpected = buildExpectedFlights(airlineCode);

            // Force a flight to have a lower price than all others in this list.
            Flight firstFlight = nextExpected.get(0);
            Flight reducedPrice = firstFlight.withPrice(1.0);
            nextExpected.set(nextExpected.indexOf(firstFlight), reducedPrice);
            bestPrice.add(reducedPrice);

            mockBackEnd.enqueue(new MockResponse()
                    .setBody(objectMapper.writeValueAsString(nextExpected))
                    .addHeader("Content-Type", "application/json"));

            expected.addAll(nextExpected);
        }

        // We know that the first flight of the combined list of expected
        // results will return the correct flights from each of the
        // airline services.
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(expected.get(0));

        // Sanity check to ensure that the expected list of best priced flights
        // will all match a request parameter taken from the first random flight.
        assertThat(bestPrice.stream().allMatch(flight ->
                flight.getDepartureAirport()
                        .equals(flightRequest.getDepartureAirport()) &&
                        flight.getDepartureDate()
                                .equals(flightRequest.getDepartureDate()) &&
                        flight.getArrivalAirport()
                                .equals(flightRequest.getArrivalAirport()))
        ).isTrue();

        // It's not possible to use a StepVerifier here because of
        // the bestPrice Min calculation interferes with the step
        // verifier resulting in a hanging state while waiting for
        // next step (non-existent step). The workaround is to block
        // and get the list.
        List<Flight> result =
                service.findBestPriceFlights(
                        flightRequest.getDepartureAirport(),
                        flightRequest.getDepartureDate(),
                        flightRequest.getArrivalAirport(),
                        flightRequest.getCurrency())
                        .collectList()
                        .block();
        assertThat(result).isNotNull();
        assertThat(new HashSet<>(result)).isEqualTo(new HashSet<>(bestPrice));
    }

    private List<Flight> buildExpectedFlights(String airlineCode) {
        List<Flight> flights = FlightFactory.builder()
                .airlines(airlineCode)
                .dailyFlights(2) // Ensures that some flights will have the same minimum price.
                .build();
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(flights.get(0));

        return flights.stream()
                .filter(flight ->
                        flight.getAirlineCode().equals(airlineCode) &&
                                flight.getDepartureAirport()
                                        .equals(flightRequest.getDepartureAirport()) &&
                                flight.getDepartureDate()
                                        .equals(flightRequest.getDepartureDate()) &&
                                flight.getArrivalAirport()
                                        .equals(flightRequest.getArrivalAirport()))
                .collect(Collectors.toList());
    }
}