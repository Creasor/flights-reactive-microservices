package server.airline;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import reactor.core.publisher.Flux;
import server.airline.aal.AALApplication;
import server.airline.common.AirlineRepository;
import server.airline.common.AirlineService;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * These use mocking to isolate and test only the service component.
 */
@AutoConfigureMockMvc
@DataR2dbcTest(properties = {"app.data=", "app.schema="})
@ContextConfiguration(classes = {AALApplication.class, AirlineService.class})
public class AirlineServiceTests {

    @Autowired
    AirlineService service;

    @MockBean
    AirlineRepository repository;

    @Test
    public void testFindFlights() {
        Flight expected = FlightFactory.randomFlight();
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(expected);

        when(repository
                .findByDepartureAirportAndDepartureDateAndArrivalAirport(
                        flightRequest.getDepartureAirport(),
                        flightRequest.getDepartureDate(),
                        flightRequest.getArrivalAirport())
        ).thenReturn(Flux.just(expected));

        Flux<Flight> flights = service.findFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport()
        );

        assertThat(flights.collectList().block()).isEqualTo(List.of(expected));

        verify(repository, times(1))
                .findByDepartureAirportAndDepartureDateAndArrivalAirport(
                        flightRequest.getDepartureAirport(),
                        flightRequest.getDepartureDate(),
                        flightRequest.getArrivalAirport());
    }

    @Test
    public void testFindBestPrice() {
        Flight expected = FlightFactory.randomFlight();
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(expected);

        when(repository
                .findByDepartureAirportAndDepartureDateAndArrivalAirport(
                        flightRequest.getDepartureAirport(),
                        flightRequest.getDepartureDate(),
                        flightRequest.getArrivalAirport())
        ).thenReturn(Flux.just(expected));

        Flux<Flight> flights = service.findFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport()
        );

        assertThat(flights.collectList().block()).isEqualTo(List.of(expected));

        verify(repository, times(1))
                .findByDepartureAirportAndDepartureDateAndArrivalAirport(
                        flightRequest.getDepartureAirport(),
                        flightRequest.getDepartureDate(),
                        flightRequest.getArrivalAirport());
    }

    @Test
    public void testFindFlightDates() {
        List<LocalDate> expected = new ArrayList<>(2);
        expected.add(LocalDate.parse("1234-01-01"));
        expected.add(LocalDate.parse("1234-01-02"));
        String from = "from here";
        String to = "to there";

        when(repository.findDepartureDates(from, to))
                .thenReturn(Flux.fromIterable(expected));

        Flux<LocalDate> departureDates = service.findDepartureDates(from, to);
        assertThat(departureDates.collectList().block()).isEqualTo(expected);

        verify(repository, times(1)).findDepartureDates(from, to);
    }
}