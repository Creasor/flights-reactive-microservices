package server.airline;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.r2dbc.dialect.H2Dialect;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.test.context.ContextConfiguration;
import reactor.test.StepVerifier;
import server.airline.aal.AALApplication;
import server.airline.common.AirlineRepository;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@DataR2dbcTest(properties = "app.data=")
@ContextConfiguration(classes = AALApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AirlineRepositoryTests {

    @Autowired
    private DatabaseClient databaseClient;

    @Autowired
    private AirlineRepository repository;

    private static final int AIRLINE_COUNT = 2;
    private static final int AIRPORT_COUNT = 3;
    private static final int NUMBER_OF_DAYS = 7;
    private static final int FLIGHTS_PER_DAY = 2;

    private List<Flight> flights;

    @BeforeAll
    public void beforeAll() {
        flights = FlightFactory.builder()
                .airlines(AIRLINE_COUNT)
                .airports(AIRPORT_COUNT)
                .from(LocalDate.now())
                .to(LocalDate.now().plusDays(NUMBER_OF_DAYS))
                .dailyFlights(FLIGHTS_PER_DAY)
                .currency("USD")
                .build();

        R2dbcEntityTemplate template =
                new R2dbcEntityTemplate(databaseClient, H2Dialect.INSTANCE);

        for (Flight flight : flights) {
            template.insert(Flight.class)
                    .using(flight)
                    .then()
                    .as(StepVerifier::create)
                    .verifyComplete();
        }
    }

    @Test
    public void testFindFlights() {
        // Tests all possible flight requests from the mock flight list.
        for (Flight nextFlight : flights) {
            FlightRequest flightRequest = FlightFactory.buildRequestFrom(nextFlight);

            List<Flight> expected = flights.stream()
                    .filter(flight ->
                            flight.getDepartureAirport().equals(
                                    flightRequest.getDepartureAirport()) &&
                                    flight.getArrivalAirport().equals(
                                            flightRequest.getArrivalAirport()) &&
                                    flight.getDepartureDate().equals(
                                            flightRequest.getDepartureDate()))
                    .collect(Collectors.toList());

            repository.findByDepartureAirportAndDepartureDateAndArrivalAirport(
                    flightRequest.getDepartureAirport(),
                    flightRequest.getDepartureDate(),
                    flightRequest.getArrivalAirport())
                    .as(StepVerifier::create)
                    .expectNextSequence(expected)
                    .verifyComplete();
        }
    }

    @Test
    public void testFindFlightDates() {
        for (Flight nextFlight : flights) {
            String departureAirport = nextFlight.getDepartureAirport();
            String arrivalAirport = nextFlight.getArrivalAirport();

            List<LocalDate> expected =
                    flights.stream()
                            .filter(flight ->
                                    flight.getDepartureAirport().equals(departureAirport) &&
                                            flight.getArrivalAirport().equals(arrivalAirport))
                            .map(Flight::getDepartureDate)
                            .distinct()
                            .collect(Collectors.toList());

            repository.findDepartureDates(departureAirport, arrivalAirport)
                    .as(StepVerifier::create)
                    .expectNextSequence(expected)
                    .verifyComplete();
        }
    }
}