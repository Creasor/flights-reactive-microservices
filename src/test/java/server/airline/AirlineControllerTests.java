package server.airline;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.AutoConfigureDataR2dbc;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import server.airline.aal.AALApplication;
import server.airline.common.AirlineController;
import server.airline.common.AirlineService;
import server.common.Utils;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static server.common.Constants.EndPoint.*;

@WebFluxTest(controllers = {AirlineController.class})
@AutoConfigureDataR2dbc
@ContextConfiguration(classes = AALApplication.class)
public class AirlineControllerTests {
    @Autowired
    WebTestClient webTestClient;

    @MockBean
    private AirlineService service;

    @Test
    public void testFindFlights() {
        Flight expected = FlightFactory.randomFlight();
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(expected);

        when(service.findFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport()
        )).thenReturn(Flux.just(expected));

        List<Flight> result = webTestClient
                .get()
                .uri("/" + FLIGHTS +
                     "?departureAirport=" + flightRequest.getDepartureAirport() +
                     "&departureDate=" + Utils.dateString(flightRequest.getDepartureDate()) +
                     "&arrivalAirport=" + flightRequest.getArrivalAirport() +
                     "&currency=" + flightRequest.getCurrency())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Flight.class)
                .returnResult()
                .getResponseBody();

        assertThat(result).isEqualTo(List.of(expected));

        verify(service, times(1)).findFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport()
        );
        clearInvocations(service);
    }

    @Test
    public void testFindBestPrice() {
        Flight expected = FlightFactory.randomFlight();
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(expected);

        when(service.findBestPrice(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport()
        )).thenReturn(Flux.just(expected));

        List<Flight> result = webTestClient
                .get()
                .uri("/" + BEST_PRICE_FLIGHTS +
                     "?departureAirport=" + flightRequest.getDepartureAirport() +
                     "&departureDate=" + Utils.dateString(flightRequest.getDepartureDate()) +
                     "&arrivalAirport=" + flightRequest.getArrivalAirport() +
                     "&currency=" + flightRequest.getCurrency())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Flight.class)
                .returnResult()
                .getResponseBody();

        assertThat(result).isEqualTo(List.of(expected));

        verify(service, times(1)).findBestPrice(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport()
        );

        clearInvocations(service);
    }

    @Test
    public void testFindFlightDates() {
        List<LocalDate> expected = new ArrayList<>(2);
        expected.add(LocalDate.parse("1234-01-01"));
        expected.add(LocalDate.parse("1234-01-02"));
        String departureAirport = "from here";
        String arrivalAirport = "to there";

        when(service.findDepartureDates(departureAirport, arrivalAirport))
                .thenReturn(Flux.fromIterable(expected));

        List<LocalDate> result = webTestClient
                .get()
                .uri("/" + FLIGHT_DATES +
                     "?departureAirport=" + departureAirport +
                     "&arrivalAirport=" + arrivalAirport)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(LocalDate.class)
                .returnResult()
                .getResponseBody();

        verify(service, times(1)).findDepartureDates(departureAirport, arrivalAirport);

        assertThat(result).isEqualTo(expected);
    }
}