package server.exchange;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import server.common.model.ExchangeRate;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@DataR2dbcTest
public class ExchangeServiceTests {
    @MockBean
    ExchangeRepository repository;

    @Autowired
    ExchangeService service;

    @Test
    public void testFindExchangeRate() {
        ExchangeRate expected = ExchangeRate.of("CAN", "USD", 0.68);

        when(repository.findByFromCurrencyAndToCurrency(
                expected.getFromCurrency(),
                expected.getToCurrency())
        ).thenReturn(Mono.just(expected));

        ExchangeRate result =
                service.getRate(
                        expected.getFromCurrency(),
                        expected.getToCurrency()
                ).block();

        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void testFindExchangeRates() {
        List<ExchangeRate> rates = List.of(
                ExchangeRate.of("CAN", "USD", 0.68),
                ExchangeRate.of("JPY", "USD", 0.0092)
        );

        when(repository.findByToCurrency("USD")).thenReturn(Flux.fromIterable(rates));

        List<ExchangeRate> result = service.getRates("USD").collectList().block();

        assertThat(result).isEqualTo(rates);
    }
}