package server.exchange;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.AutoConfigureDataR2dbc;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import server.common.model.ExchangeRate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static server.common.Constants.EndPoint.RATE;
import static server.common.Constants.EndPoint.RATES;

@WebFluxTest(controllers = {ExchangeController.class})
@AutoConfigureDataR2dbc
public class ExchangeControllerTests {

    @Autowired
    WebTestClient webTestClient;

    @MockBean
    private ExchangeService service;

    @Test
    public void testGetRate() {
        ExchangeRate expected = ExchangeRate.of("ABC", "DEF", 0.7);

        when(service.getRate(
                expected.getFromCurrency(),
                expected.getToCurrency())
        ).thenReturn(Mono.just(expected));

        ExchangeRate result = webTestClient.get()
                .uri("/" + RATE +
                      "?fromCurrency=" + expected.getFromCurrency() +
                      "&toCurrency=" + expected.getToCurrency())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ExchangeRate.class)
                .returnResult()
                .getResponseBody();

        verify(service, times(1)).getRate(expected.getFromCurrency(), expected.getToCurrency());

        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void testGetRates() {
        ExchangeRate[] rates = new ExchangeRate[]{
                ExchangeRate.of("CAN", "USD", 0.68),
                ExchangeRate.of("JPY", "USD", 0.0092)};

        when(service.getRates("USD")).thenReturn(Flux.fromArray(rates));

        ExchangeRate[] result = webTestClient.get()
                .uri("/" + RATES + "?toCurrency=USD")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ExchangeRate[].class)
                .returnResult()
                .getResponseBody();

        verify(service, times(1)).getRates("USD");

        assertThat(result).isEqualTo(rates);
    }
}