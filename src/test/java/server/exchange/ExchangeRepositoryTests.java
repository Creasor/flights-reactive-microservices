package server.exchange;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.r2dbc.dialect.H2Dialect;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.test.context.ContextConfiguration;
import reactor.test.StepVerifier;
import server.common.model.ExchangeRate;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataR2dbcTest(properties = "app.data=")
@ContextConfiguration(classes = ExchangeApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ExchangeRepositoryTests {

    @Autowired
    private DatabaseClient databaseClient;

    @Autowired
    private ExchangeRepository repository;

    private List<ExchangeRate> rates;

    R2dbcEntityTemplate template;

    @BeforeAll
    public void beforeAll() {
        rates = List.of(
                ExchangeRate.of("USD", "CAN", 1.32),
                ExchangeRate.of("USD", "JPY", 108.98),
                ExchangeRate.of("CAN", "USD", 0.68),
                ExchangeRate.of("CAN", "JPY", 87.36),
                ExchangeRate.of("JPY", "USD", 0.0092),
                ExchangeRate.of("JPY", "CAN", 0.011)
        );

        template = new R2dbcEntityTemplate(databaseClient, H2Dialect.INSTANCE);

        for (ExchangeRate exchangeRate : rates) {
            template.insert(ExchangeRate.class)
                    .using(exchangeRate)
                    .then()
                    .as(StepVerifier::create)
                    .verifyComplete();
        }
    }

    @Test
    public void testFindByFromCurrencyAndToCurrency() {
        for (ExchangeRate exchangeRate : rates) {
            repository.findByFromCurrencyAndToCurrency(exchangeRate.getFromCurrency(), exchangeRate.getToCurrency())
                    .as(StepVerifier::create)
                    .assertNext(actual -> {
                        assertThat(actual.getFromCurrency()).isEqualTo(exchangeRate.getFromCurrency());
                        assertThat(actual.getToCurrency()).isEqualTo(exchangeRate.getToCurrency());
                        assertThat(actual.getRate()).isEqualTo(exchangeRate.getRate());
                    })
                    .verifyComplete();
        }
    }

    @Test
    public void testFindByToCurrency() {
        List<ExchangeRate> usd = repository.findByToCurrency("USD").collectList().block();
        repository.findByToCurrency("USD")
                .as(StepVerifier::create)
                .assertNext(actual -> {
                    assertThat(actual.getFromCurrency()).isEqualTo("CAN");
                    assertThat(actual.getToCurrency()).isEqualTo("USD");
                })
                .assertNext(actual -> {
                    assertThat(actual.getFromCurrency()).isEqualTo("JPY");
                    assertThat(actual.getToCurrency()).isEqualTo("USD");
                })
                .verifyComplete();
    }
}