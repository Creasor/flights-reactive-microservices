DROP TABLE IF EXISTS FLIGHT;
CREATE TABLE FLIGHT (
    id SERIAL PRIMARY KEY,
    departure_airport VARCHAR(255) NOT NULL,
    departure_date DATE NOT NULL,
    departure_time TIME NOT NULL,
    arrival_airport VARCHAR(255) NOT NULL,
    arrival_date DATE NOT NULL,
    arrival_time TIME NOT NULL,
    kilometers INT NOT NULL,
    price DOUBLE NOT NULL,
    currency VARCHAR(3) NOT NULL,
    airline_code VARCHAR(255) NOT NULL
);