insert into FLIGHT (
	id,
	departure_airport,
	departure_date,
	departure_time,
	arrival_airport,
	arrival_date,
	arrival_time,
	kilometers,
	price,
	currency,
	airline_code
) values
	(default, '1', '2021-03-14', '07:17', '2', '2021-03-14', '15:14', 3183, 632.0, 'USD', 'SWA'),
	(default, '2', '2021-03-14', '05:02', '1', '2021-03-14', '12:59', 3183, 748.0, 'USD', 'SWA'),
	(default, '1', '2021-03-14', '00:01', '3', '2021-03-14', '05:46', 2299, 354.0, 'USD', 'SWA'),
	(default, '3', '2021-03-14', '09:35', '1', '2021-03-14', '15:20', 2299, 520.0, 'USD', 'SWA'),
	(default, '2', '2021-03-14', '05:15', '3', '2021-03-14', '13:13', 3191, 931.0, 'USD', 'SWA'),
	(default, '3', '2021-03-14', '05:29', '2', '2021-03-14', '13:27', 3191, 637.0, 'USD', 'SWA'),
	(default, '1', '2021-03-15', '05:38', '2', '2021-03-15', '13:35', 3183, 841.0, 'USD', 'SWA'),
	(default, '2', '2021-03-15', '03:31', '1', '2021-03-15', '11:28', 3183, 586.0, 'USD', 'SWA'),
	(default, '1', '2021-03-15', '04:37', '3', '2021-03-15', '10:22', 2299, 527.0, 'USD', 'SWA'),
	(default, '3', '2021-03-15', '03:23', '1', '2021-03-15', '09:08', 2299, 462.0, 'USD', 'SWA'),
	(default, '2', '2021-03-15', '05:15', '3', '2021-03-15', '13:13', 3191, 702.0, 'USD', 'SWA'),
	(default, '3', '2021-03-15', '03:48', '2', '2021-03-15', '11:46', 3191, 455.0, 'USD', 'SWA'),
	(default, '1', '2021-03-16', '13:46', '2', '2021-03-16', '21:43', 3183, 760.0, 'USD', 'SWA'),
	(default, '2', '2021-03-16', '09:04', '1', '2021-03-16', '17:01', 3183, 455.0, 'USD', 'SWA'),
	(default, '1', '2021-03-16', '02:19', '3', '2021-03-16', '08:04', 2299, 411.0, 'USD', 'SWA'),
	(default, '3', '2021-03-16', '00:19', '1', '2021-03-16', '06:04', 2299, 298.0, 'USD', 'SWA'),
	(default, '2', '2021-03-16', '15:31', '3', '2021-03-16', '23:29', 3191, 483.0, 'USD', 'SWA'),
	(default, '3', '2021-03-16', '10:22', '2', '2021-03-16', '18:20', 3191, 556.0, 'USD', 'SWA'),
	(default, '1', '2021-03-17', '11:37', '2', '2021-03-17', '19:34', 3183, 503.0, 'USD', 'SWA'),
	(default, '2', '2021-03-17', '01:33', '1', '2021-03-17', '09:30', 3183, 797.0, 'USD', 'SWA'),
	(default, '1', '2021-03-17', '14:49', '3', '2021-03-17', '20:34', 2299, 659.0, 'USD', 'SWA'),
	(default, '3', '2021-03-17', '16:53', '1', '2021-03-17', '22:38', 2299, 700.0, 'USD', 'SWA'),
	(default, '2', '2021-03-17', '00:53', '3', '2021-03-17', '08:51', 3191, 466.0, 'USD', 'SWA'),
	(default, '3', '2021-03-17', '09:05', '2', '2021-03-17', '17:03', 3191, 943.0, 'USD', 'SWA');