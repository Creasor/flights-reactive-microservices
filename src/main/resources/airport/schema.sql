DROP TABLE IF EXISTS airport;
CREATE TABLE airport (
    airport_code VARCHAR(4) PRIMARY KEY,
    airport_name VARCHAR(100) NOT NULL
);