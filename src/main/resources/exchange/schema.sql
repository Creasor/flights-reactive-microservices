DROP TABLE IF EXISTS exchange_rate;
CREATE TABLE exchange_rate (
    id SERIAL PRIMARY KEY,
    from_currency VARCHAR(3) NOT NULL,
    to_currency VARCHAR(3) NOT NULL,
    rate DOUBLE NOT NULL
);