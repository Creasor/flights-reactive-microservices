package server.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * This "Plain Old Java Object" (POJO) class defines information about
 * an airport.
 *
 * The {@code @Data} annotation generates all the boilerplate that is
 * normally associated with simple POJOs (Plain Old Java Objects) and
 * getters for all fields, setters for all non-final fields, and
 * appropriate toString, equals and hashCode implementations that
 * involve the fields of the class, and a constructor that initializes
 * all final fields, as well as all non-final fields with no
 * initializer that have been marked with @NonNull, to ensure the
 * field is never null.
 *
 * The {@code @AllArgsConstructor} annotation generates an all-args
 * constructor that includes one argument for every field in the
 * class.
 *
 * The {@code @NoArgsConstructor} will generate a constructor with no
 * parameter. The @Entity annotation specifies that this class is an
 * entity and is mapped to a database table.
 * 
 * The {@code @Entity} annotation specifies that this class is an
 * entity and is mapped to a database table.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Entity
public class Airport {
    /**
     * The IAOC 4 letter airport code is the unique primary key.  The
     * {@code @Id} annotation indicates the {@code id} field below is
     * the primary key of an {@link Airport} object.  The
     * {@code @Column} annotation customizes the mapping between the
     * entity attribute and the database column, which in this case
     * limits the length of String-valued database column to 4
     * characters.
     */
    @Id
    @Column(length = 4)
    String airportCode;

    /**
     * The airport name (e.g., city, state, and country).
     */
    String airportName;
}
