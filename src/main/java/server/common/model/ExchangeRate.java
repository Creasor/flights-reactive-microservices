package server.common.model;

import lombok.*;
import org.springframework.data.annotation.Id;

import javax.persistence.*;

/**
 * This "Plain Old Java Object" (POJO) class keeps track of currency
 * conversion rates.
 *
 * The {@code @Data} annotation generates all the boilerplate that is
 * normally associated with simple POJOs (Plain Old Java Objects) and
 * getters for all fields, setters for all non-final fields, and
 * appropriate toString, equals and hashCode implementations that
 * involve the fields of the class, and a constructor that initializes
 * all final fields, as well as all non-final fields with no
 * initializer that have been marked with {@code @NonNull}, to ensure
 * the field is never null.
 *
 * The {@code @AllArgsConstructor} annotation generates an all-args
 * constructor that includes one argument for every field in the
 * class.
 *
 * The {@code @NoArgsConstructor} will generate a constructor with no
 * parameter. The @Entity annotation specifies that this class is an
 * entity and is mapped to a database table.
 *
 * The {@code @With} annotation generates a method that constructs a
 * clone of the object, but with a new value for this one field.
 *
 * The {@code @Builder} annotation automatically creates a static
 * builder factory method for this class that can be used as follows:
 *
 * CurrencyConversion conversion = CurrencyConversion
 * .builder()
 * .to("CAN")
 * .from("US")
 * .build();
 */
@Data
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder
@With
@Entity
public class ExchangeRate {
    /**
     * The {@code @Id} annotation indicates the {@code id} field below
     * is the primary key of an {@link ExchangeRate} object.  The
     * {@code @GeneratedValue} annotation configures the specified
     * field to auto-increment.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The name of the currency to convert from.
     */
    @Column(length = 3)
    String fromCurrency;

    /**
     * The name of the currency to convert to.
     */
    @Column(length = 3)
    String toCurrency;

    /**
     * The exchange rate for the "from" currency to the "to" currency.
     */
    double rate;

    /**
     * This factory method initializes an {@link ExchangeRate} from
     * the given parameters.
     *
     * @param fromCurrency The 3 letter currency code to convert from
     * @param toCurrency   The 3 letter currency code to convert to
     * @param rate         The exchange rate
     * @return An initialized {@link ExchangeRate} object
     */
    public static ExchangeRate of(String fromCurrency, String toCurrency, double rate) {
        return new ExchangeRate(null, fromCurrency, toCurrency, rate);
    }
}
