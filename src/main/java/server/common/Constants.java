package server.common;

/**
 * Static class used to centralize all constants used by the
 * server and all microservices.
 *
 * All http requests urls use the following convention:
 *
 * http:port//service/endpoint/strategy
 *
 * where "service" is one of the named microservices in the {@link
 * Service} inner class, "endpoint" is one of the endpoints declared
 * in the {@link EndPoint} inner class.
 */
public class Constants {
    /**
     * Each microservice will automatically register itself with the
     * Eureka service using it's unique {@link Service} name string
     * and a randomly generated port. Http requests can then use the
     * microservice name instead of an ip address and port number.
     *
     * How redirection works in practice:
     *
     * When the SWA airline microservice starts up, Spring will
     * allocate an unused random port (signaled by the server.port=0
     * entry in the application.properties file). This port is then
     * used when registering the microservice with the Eureka
     * discovery service (invoked by the @EnableDiscoveryClient
     * application annotation). Once registered, all requests to
     * microservices can simply use their registered application
     * names.
     *
     * For example, if the aal-airline microservice is allocated port
     * 40928, then a flights request url will be mapped by the Eureka
     * server as follows:
     *
     * http://aal-airline/flights -> 192.168.7.23:40928/flights
     */
    public static class Service {
        /**
         * All service names must match the spring.application.name
         * property in each microservice application properties
         * resource file.
         *
         * All airline services must include "airline" in their names
         * so that the FlightServices class will be able to determine
         * which microservices support flight and best-price requests.
         */
        public static final String AIRPORT = "airport";
        public static final String EXCHANGE = "exchange";
        public static final String AA_AIRLINE = "aal-airline";
        public static final String SWA_AIRLINE = "swa-airline";
    }

    /**
     * All supported HTTP request end-points.
     */
    public static class EndPoint {
        public static final String RATE = "rate";
        public static final String RATES = "rates";
        public static final String AIRPORTS = "airports";
        public static final String BEST_PRICE_FLIGHTS = "best-price";
        public static final String FLIGHTS = "flights";
        public static final String FLIGHT_DATES = "dates";
    }

    /**
     * Common resource file names used by all microservices.
     */
    public static class Resources {
        public static final String EUREKA_CLIENT_PROPERTIES =
                "classpath:/common/eureka-client.properties";
        public static final String DATABASE_PROPERTIES =
                "classpath:/common/database.properties";
    }
}
