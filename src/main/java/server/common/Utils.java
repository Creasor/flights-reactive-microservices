package server.common;

import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * This Java utility class defines methods shared by other parts of
 * the app.
 */
public final class Utils {
    /**
     * A Java utility class should have a private constructor.
     */
    private Utils() {
    }

    /**
     * Format the {@link LocalDate} param to use {@code
     * ISO_LOCAL_DATE} format.
     */
    public static String dateString(LocalDate localDate) {
        return localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    /**
     * Make an HTTP GET call to the server passing in the {@code url}
     * and returning a result of type {@code Flux<T>}.
     *
     * @param url The URL to pass to the server via a GET request
     * @param clazz The type {@code T} to return from GET
     * @return The result of type {@code Flux<T>} from the server
     */
    public static <T> Flux<T> makeGetRequestFlux(WebClient webClient,
                                                 String url,
                                                 Class<T> clazz) {
        return webClient
            // Create an HTTP GET request.
            .get()

            // Build an airline GET request.
            .uri(url)

            // Retrieve the response from the microservice.
            .retrieve()

            // Convert body to a Flux<clazz> object.
            .bodyToFlux(clazz);
    }

    /**
     * Make an HTTP GET call to the server passing in the {@code url}
     * and returning a result of type {@code Mono<T>}.
     *
     * @param url The URL to pass to the server via a GET request
     * @param clazz The type {@code T} to return from GET
     * @return The result of type {@code Mono<T>} from the server
     */
    public static <T> Mono<T> makeGetRequestMono(WebClient webClient,
                                                 String url,
                                                 Class<T> clazz) {
        return webClient
            // Create an HTTP GET request.
            .get()

            // Build an airline GET request.
            .uri(url)

            // Retrieve the response from the microservice.
            .retrieve()

            // Convert body to a Mono<clazz> object.
            .bodyToMono(clazz);
    }
}
