package server.airport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import server.common.model.Airport;
import reactor.core.publisher.Flux;

import static server.common.Constants.EndPoint.AIRPORTS;

/**
 * This Spring controller demonstrates how Spring MVC can be used to
 * handle HTTP GET requests via Java reactive programming.  These
 * requests are mapped to an endpoint method that forwards to the
 * {@link AirportService}, which returns a {@link Flux} that emits all
 * known {@link Airport} objects asynchronously.
 *
 * In Spring's approach to building RESTful web services, HTTP
 * requests are handled by a controller (identified by the
 * {@code @RestController} annotation) that defines the endpoints (aka
 * routes) for each supported operation, i.e., {@code @GetMapping},
 * {@code @PostMapping}, {@code @PutMapping}, and {@code 
 * @DeleteMapping}, which correspond to the HTTP GET, POST, PUT, and
 * DELETE calls, respectively.
 *
 * Spring uses the {@code @GetMapping} annotation to map HTTP GET
 * requests onto methods in the {@link AirportController}.  GET
 * requests invoked from any HTTP web client (e.g., a web browser or
 * Android app) or command-line utility (e.g., Curl or Postman).
 *
 * The {@code @ResponseBody} annotation tells a controller that the
 * object returned is automatically serialized into JSON and passed
 * back into the HttpResponse object.  The {@code @CrossOrigin}
 * annotation marks the annotated method or type as permitting cross
 * origin requests, which is required for Eureka redirection.
 */
@RestController
@CrossOrigin("*") // Required for Eureka redirection
@ResponseBody
public class AirportController {
    /**
     * This auto-wired field connects the {@link AirportController} to
     * the {@link AirportService}.
     */
    @Autowired
    AirportService service;

    /**
     * Returns a {@link Flux} that emits all known airports
     * asynchronously.
     *
     * @return A {@link Flux} that emits all supported airports
     * asynchronously
     */
    @GetMapping(AIRPORTS)
    public Flux<Airport> getAirports() {
        return service
            // Forward to the service.
            .getAirports();
    }
}
