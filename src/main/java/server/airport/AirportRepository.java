package server.airport;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import server.common.model.Airport;

/**
 * This class defines an implementation method that is called by the
 * {@link AirportController}.  This method returns a {@link Flux} that
 * emits all known {@link Airport} objects asynchronously.
 * 
 * It is annotated as a Spring {@code @Service}, which enables the
 * auto-detection and wiring of dependent implementation classes via
 * classpath scanning.
 */
@Repository
public interface AirportRepository
       extends ReactiveCrudRepository<Airport, String> {
    /**
     * This class inherits a findAll() method from the {@link
     * ReactiveCrudRepository}.
     */
}
