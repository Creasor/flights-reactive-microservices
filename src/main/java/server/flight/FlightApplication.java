package server.flight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

/**
 * The flight application is the "front-end" app gateway that forwards
 * incoming HTTP requests from clients to microservices in the Flight
 * Listing App (FLApp).  These requests are mapped to asynchronous
 * methods that use the Project Reactor reactive streams framework to
 * find all available flights, find the best price for a flight
 * request, get a list of airports, and find departure dates for a
 * given pair of airports.  This class also plays the role of a Eureka
 * "client" wrt microservice discovery.
 * 
 * The {@code @SpringBootApplication} annotation enables apps to use
 * auto-configuration, component scan, and to define extra
 * configurations on their "application" class.
 *
 * The {@code @EnableDiscoveryClient} annotation enables service
 * registration and discovery.  However, this app uses a fixed port
 * specified in the resources {@code application.properties} file and
 * does not need to register itself with the eureka discovery client,
 * so {@code autoRegister} is set to false.
 *
 * The {@code @ComponentScan} annotation tells Spring the packages to
 * scan for annotated components (i.e., tagged with {@code 
 * @Component}).
 *
 * The {@code @PropertySource} annotation is used to provide a
 * properties file to the Spring Environment.
 */
@SpringBootApplication
@EnableDiscoveryClient(autoRegister = false)
@ComponentScan({"server.common", "server.flight"})
@PropertySource("classpath:/flight/flight.properties")
public class FlightApplication {
    /**
     * A static main() entry point is needed to run the reactive
     * Flight Listing App (FLApp).
     */
    public static void main(String[] args) {
        // Launch the Flight "front-end" app gateway within Spring
        // WebFlux.
        SpringApplication.run(FlightApplication.class, args);
    }
}
