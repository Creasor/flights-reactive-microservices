package server.flight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import server.common.CheapestPriceCollector;
import server.common.Utils;
import server.common.model.Airport;
import server.common.model.ExchangeRate;
import server.common.model.Flight;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static server.common.Constants.EndPoint.*;
import static server.common.Constants.Service.AIRPORT;
import static server.common.Constants.Service.EXCHANGE;

/**
 * This class defines implementation methods that are called by the
 * {@link FlightController}, which serves as the main "front-end" app
 * gateway entry point for remote clients.  These methods use Project
 * Reactor reactive types to asynchronously find all available
 * flights, find the best price for a flight request, get a list of
 * airports, and find departure dates for a given pair of airports.
 *
 * This class is annotated as a Spring {@code @Service}, which enables
 * the auto-detection and wiring of dependent implementation classes
 * via classpath scanning.
 *
 * A {@link DiscoveryClient} is used to redirect calls to the
 * appropriate microservices, which can run in processes that are
 * deployed to other computers in a cluster.
 */
@Service
public class FlightService {
    /**
     * This auto-wired field connects the {@link FlightService} to
     * the {@link DiscoveryClient} used to find all registered
     * microservices.
     */
    @Autowired
    DiscoveryClient discoveryClient;

    /**
     * This auto-wired field connects the {@link FlightService} to a
     * {@link WebClient} that is used to redirect all HTTP requests to
     * the appropriate microservices.
     */
    @Autowired
    WebClient webClient;

    /**
     * The base URL used for all redirections.  Tests can set this
     * value for mocking a back-end server.
     */
    public String baseUrl = "http://";

    /**
     * Find the {@link Airport} objects that describe the supported
     * airports.
     *
     * @return A {@link Flux} that emits all the supported {@link
     * Airport} objects
     */
    public Flux<Airport> getAirports() {
        return Utils
            // Make an asynchronous HTTP GET request and return a Flux
            // to an ExchangeRate object.
            .makeGetRequestFlux(webClient,
                                makeAirportsUrl(),
                                Airport.class);
    }

    /**
     * Finds all departure dates that have at least one flight running
     * from the {@code departureAirport} to the {@code
     * arrivalAirport}.
     *
     * @param departureAirport The 3 letter departure airport code
     * @param arrivalAirport   The 3 letter arrival airport code
     * @return A {@link Flux} that emits all matching flight dates
     */
    public Flux<LocalDate> findDepartureDates(String departureAirport,
                                              String arrivalAirport) {
        return
            // Asynchronously get a Flux of all registered airline
            // microservices registered with the discovery service.
            getAirlineServices()

            // Use the flatMap() concurrency idiom to (1) find all
            // matching flights for each airline microservice and (2)
            // combine all results into a single Flux of matching
            // Flight results.
            .flatMap(airline ->
                     findAirlineDepartureDates(departureAirport,
                                               arrivalAirport,
                                               airline));
    }

    /**
     * Find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price(s) to use the
     * desired {@code toCurrency}.
     *
     * @param departureAirport The 3 letter departure airport code
     * @param departureDate The departure date
     * @param arrivalAirport The 3 letter arrival airport code
     * @param toCurrency The 3 letter currency to convert to (if
     *                   necessary)
     * @return A {@link Flux} that emits matching {@link Flight}
     *         objects in the desired currency
     */
    public Flux<Flight> findFlights(String departureAirport,
                                    LocalDate departureDate,
                                    String arrivalAirport,
                                    String toCurrency) {
        return findFlightsImpl(departureAirport,
                               FLIGHTS,
                               departureDate,
                               arrivalAirport,
                               toCurrency);
    }

    /**
     * Find the best priced flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price to use the desired
     * {@code toCurrency}.
     *
     * @param departureAirport The 3 letter departure airport code
     * @param departureDate The date of the departure
     * @param arrivalAirport The 3 letter arrival airport code
     * @param toCurrency The 3 letter currency to convert to (if
     *                   necessary)
     * @return A {@link Flux} that emits the {@link Flight} object(s)
     *         with the best price in the desired currency
     */
    public Flux<Flight> findBestPriceFlights(String departureAirport,
                                             LocalDate departureDate,
                                             String arrivalAirport,
                                             String toCurrency) {
        // Return a Flux that emits Flight object(s) with the best
        // price.
        return
            // Find all flights matching the specified params.
            findFlightsImpl(departureAirport,
                            BEST_PRICE_FLIGHTS,
                            departureDate,
                            arrivalAirport,
                            toCurrency)
                        
            // Converts a Flux of Flights into a new Mono to a Flux
            // that emits the cheapest priced trips(s).
            .collect(CheapestPriceCollector.toFlux())

            // Unwrap Mono and return the Flux<Flight> that emits the
            // cheapest priced trip(s) in the desired currency.
            .flatMapMany(Function.identity());
    }

    /**
     * Use the operation designated by the HTTP {@code endpoint} to
     * asynchronously find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price(s) to use the
     * desired {@code toCurrency}.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport The arrival airport
     * @param toCurrency The desired currency
     * @param endpoint The HTTP endpoint operation to perform
     * @return A {@link Flux} that emits matching {@link Flight}
     *         objects in the desired currency
     */
    public Flux<Flight> findFlightsImpl(String departureAirport,
                                        String endpoint,
                                        LocalDate departureDate,
                                        String arrivalAirport,
                                        String toCurrency) {
        // The following two local variables are initialized
        // asynchronously via Project Reactor parallel features.

        Mono<Map<String, Double>> rates =
            // Asynchronously get a Flux of ExchangeRates from the
            // ExchangeRate microservice that has registered with the
            // discovery service.
            getRates(toCurrency)

            // Convert the Flux of rates to a Map using fromCurrency
            // as the key and the exchange rate as the value.
            .collectMap(ExchangeRate::getFromCurrency,
                        ExchangeRate::getRate);

        Flux<Flight> flights =
            // Asynchronously get a Flux of all registered airline
            // microservices registered with the discovery service.
            getAirlineServices()

            // Use the flatMap() concurrency idiom to (1) find all
            // matching flights for each airline microservice and (2)
            // combine all results into a single Flux of matching
            // Flight results.
            .flatMap(airline ->
                     findAirlineFlights(airline,
                                        endpoint,
                                        departureAirport,
                                        departureDate,
                                        arrivalAirport));

        // Return a Flux that emits flights with the correct prices
        // after all asynchronous computations complete.
        return tryToConvertCurrency(rates, flights, toCurrency);
    }

    /**
     * Attempt to convert the price of all the {@link Flight} objects
     * in {@code flights} to the desired {@code toCurrency} via the
     * {@code rates} {@link Map}, but do nothing for any flights that
     * are already in the desired currency.
     *
     * @param toCurrency The currency to convert to
     * @param ratesMap A {@link Mono} to a {@link Map} whose key is
     *                   the currency and whose value is the {@link
     *                   List} of {@link Flight} objects whose price
     *                   is given in that currency
     * @param flights A {@link Flux} of matching {@link Flight}
     *                objects
     * @return A {@link Flux} of {@link Flight} objects whose price is
     *         now in {@code toCurrency} format
     */
    private Flux<Flight> tryToConvertCurrency(Mono<Map<String, Double>> ratesMap,
                                              Flux<Flight> flights,
                                              String toCurrency) {
        return ratesMap
            // Unwrap the contained Map and use its contents to return
            // a Flux<Flight> with updated prices (if necessary).
            .flatMapMany(map -> flights
                         .map(flight -> flight
                              .getCurrency().equals(toCurrency)

                              // Flight price is in the correct
                              // currency, so this is a no-op.
                              ? flight

                              // Create new Flight with price updated
                              // from fromCurrency to toCurrency.
                              : flight.withPrice(flight.getPrice()
                                                 * map.get(flight.getCurrency()))));
    }

    /**
     * Find the exchange rate that matches the {@code url} parameter.
     *
     * @param fromCurrency The 3 letter currency code to exchange from
     * @param toCurrency The 3 letter currency code to exchange to
     * @return A {@link Mono} that emits the {@link Double} exchange
     *         rate value
     */
    public Mono<ExchangeRate> getRate(String fromCurrency, String toCurrency) {
        // Return a Mono that emits the Double exchange rate value.
        return Utils
            .makeGetRequestMono(webClient,
                                makeExchangeRateUrl(fromCurrency, toCurrency),
                                ExchangeRate.class);
    }

    /**
     * Find the exchange rate matching the {@code toCurrency} param
     * asynchronously.
     *
     * @param toCurrency The 3 letter currency code to exchange to
     * @return A {@link Mono} containing the {@link Double} exchange
     *         rate value
     */
    public Flux<ExchangeRate> getRates(String toCurrency) {
        // Return a Mono containing the Double exchange rate value.
        return Utils
            // Make an asynchronous HTTP GET request and return a Flux
            // to an ExchangeRate object.
            .makeGetRequestFlux(webClient,
                                makeExchangeRatesUrl(toCurrency),
                                ExchangeRate.class);
    }

    /**
     * Return a {@link Flux} that emits all {@link Flight} objects
     * matching the given {@code airline} param for a single airline.
     *
     * @param airline A URL that encodes all the parameters for a GET
     *                request
     * @param departureAirport The 3 letter departure airport code
     * @param departureDate The date of the departure
     * @param arrivalAirport The 3 letter arrival airport code
     * @return A {@link Flux} that emits {@link Flight} objects
     *         matching the provided {@code airline}
     */
    private Flux<Flight> findAirlineFlights(String airline,
                                            String endpoint,
                                            String departureAirport,
                                            LocalDate departureDate,
                                            String arrivalAirport) {
        // Return a Flux that emits Flight objects matching the
        // provided airline.
        return Utils
            // Make an asynchronous HTTP GET request and return a Flux
            // to a Flight object.
            .makeGetRequestFlux(webClient,
                                makeAirlineUrl(airline,
                                               endpoint,
                                               departureAirport,
                                               departureDate,
                                               arrivalAirport),
                                Flight.class);
    }

    /**
     * Finds all departure dates that have at least one flight running
     * from the {@code departureAirport} to the {@code arrivalAirport}
     * for the specified {@code airline}.
     *
     * @param departureAirport The 3 letter departure airport code
     * @param arrivalAirport The 3 letter arrival airport code
     * @param airline The 3 letter airline code
     * @return A {@link Flux} that emits all matching flight dates
     */
    public Flux<LocalDate> findAirlineDepartureDates(String departureAirport,
                                                     String arrivalAirport,
                                                     String airline) {
        // Return a Flux that emits all matching flight dates.
        return Utils
            // Make an asynchronous HTTP GET request and return a Flux
            // to a LocalDate object.
            .makeGetRequestFlux(webClient,
                                makeAirlineUrl(airline,
                                               FLIGHT_DATES,
                                               departureAirport,
                                               null, 
                                               arrivalAirport),
                                LocalDate.class);
    }

    /**
     * Uses the Eureka discovery client to finds all "airline"
     * microservices.
     *
     * @return A {@link Flux} that emits all registered "airline"
     *         microservices
     */
    private Flux<String> getAirlineServices() {
        return Flux
            // Convert the list of all Eureka registered microservices
            // into a Flux.
            .fromIterable(discoveryClient.getServices())

            // Only emit microservices pertaining to an "airline".
            .filter(id -> id.toLowerCase().contains("airline"));
    }

    /**
     * Build a GET request URL to forward to the designated Airline
     * microservice.
     *
     * @param airlineMicroservice The microservice to forward the
     *                            request to
     * @param endpointMethod The endpoint method being called
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport The arrival airport
     * @return A {@link String} containing the URL to send via an HTTP 
     *         GET request to the {@code airlineMicroservice}
     */
    private String makeAirlineUrl(String airlineMicroservice,
                                  String endpointMethod,
                                  String departureAirport,
                                  LocalDate departureDate,
                                  String arrivalAirport) {
        // Build a GET request URL along with query parameters to
        // forward to the designated Airline microservice.
        return baseUrl
            + airlineMicroservice
            + "/" 
            + endpointMethod
            + "?departureAirport=" 
            + departureAirport  
            + (departureDate != null
               ? ("&departureDate=" + departureDate)
               : (""))
            + "&arrivalAirport=" 
            + arrivalAirport;
    }

    /**
     * Make an HTTP GET request on the {@link ExchangeRate}
     * microservice via Eureka redirection.
     * 
     * @param fromCurrency The currency to convert from
     * @param toCurrency The currency to convert to
     * @return A {@link String} containing the URL to send via an HTTP
     *         GET request to the {@link ExchangeRate} microservice
     */
    private String makeExchangeRateUrl(String fromCurrency,
                                       String toCurrency) {
        return baseUrl
            + EXCHANGE
            + "/" 
            + RATE
            + "?fromCurrency" 
            + fromCurrency
            + "&toCurrency" 
            + toCurrency;
    }

    /**
     * Make an HTTP GET request on the {@link ExchangeRate}
     * microservice via Eureka redirection.
     * 
     * @param toCurrency The currency to convert to
     * @return A {@link String} containing the URL to send via an HTTP
     *         GET request to the {@link ExchangeRate} microservice
     */
    private String makeExchangeRatesUrl(String toCurrency) {
        return baseUrl
            + EXCHANGE
            + "/" 
            + RATES
            + "&toCurrency" 
            + toCurrency;
    }

    /**
     * Build an AIRPORT request URL to forward to the {@link Airport}
     * microservice.
     *
     * @return A {@link String} containing the URL to send via an HTTP
     *         GET request to the {@link Airport} microservice
     */
    private String makeAirportsUrl() {
        return baseUrl 
            + AIRPORT 
            + "/" 
            + AIRPORTS;
    }
}
