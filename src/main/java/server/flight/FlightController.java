package server.flight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import server.common.model.Airport;
import server.common.model.ExchangeRate;
import server.common.model.Flight;

import java.time.LocalDate;

import static server.common.Constants.EndPoint.*;

/**
 * This Spring controller is the main entry point for remote clients.
 * It demonstrates how Spring can be used to handle HTTP GET requests
 * synchronously via reactive programming.  These requests are mapped
 * to endpoint methods that forward to {@link FlightService} methods,
 * which asynchronously find all available flights, find the best
 * price for a flight request, get a list of airports, and find
 * departure dates for a given pair of airports.
 *
 * In Spring's approach to building RESTful web services, HTTP
 * requests are handled by a controller (identified by the
 * {@code @RestController} annotation) that defines the endpoints (aka
 * routes) for each supported operation, i.e., {@code @GetMapping},
 * {@code @PostMapping}, {@code @PutMapping}, and
 * {@code @DeleteMapping}, which correspond to the HTTP GET, POST,
 * PUT, and DELETE calls, respectively.
 *
 * Spring uses the {@code @GetMapping} annotation to map HTTP GET
 * requests onto methods in the {@link FlightController}.  GET
 * requests invoked from any HTTP web client (e.g., a web browser or
 * Android app) or command-line utility (e.g., Curl or Postman).
 */
@RestController
@CrossOrigin("*") // Required for Eureka redirection
public class FlightController {
    /**
     * This auto-wired field connects the {@link FlightController} to
     * the {@link FlightService}.
     */
    @Autowired
    FlightService service;

    /**
     * Find all {@link Airport} objects that describe the supported
     * airports.
     *
     * @return A {@link Flux} that emits {@link Airport} objects
     */
    @GetMapping(AIRPORTS)
    public Flux<Airport> getAirports() {
        return service
            // Call flight service to redirect the request to the
            // Airports microservice.
            .getAirports();
    }

    /**
     * Finds all departure dates that have at least one flight
     * running from the departure airport to the arrival airport.
     *
     * @param departureAirport The 3 letter departure airport code
     * @param arrivalAirport   The 3 letter arrival airport coe
     * @return A {@link Flux} that emits all available flight dates
     */
    @GetMapping(FLIGHT_DATES)
    Flux<LocalDate> findDepartureDates(@RequestParam String departureAirport,
                                       @RequestParam String arrivalAirport) {
        // Return a Flux that emits all the available flight dates.
        return service
            // Forward to the FlightService.
            .findDepartureDates(departureAirport, arrivalAirport);
    }

    /**
     * Find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price(s) to the desired
     * {@code currency}.
     *
     * @param departureAirport The 3 letter departure airport code
     * @param departureDate    The date of the departure
     * @param arrivalAirport   The 3 letter arrival airport code
     * @param currency         The 3 letter currency to convert to (if necessary)
     * @return A {@link Flux} that emits matching {@link Flight} objects
     */
    @GetMapping(FLIGHTS)
    public Flux<Flight> findFlights(@RequestParam String departureAirport,
                                    @RequestParam String departureDate,
                                    @RequestParam String arrivalAirport,
                                    @RequestParam String currency) {
        return service
            // Return a Flux that emits matching Flight objects.
            .findFlights(departureAirport,
                         LocalDate.parse(departureDate),
                         arrivalAirport,
                         currency);
    }

    /**
     * Find the best priced flight(s) that match the given airports
     * and departure date.
     *
     * @param departureAirport The 3 letter departure airport code
     * @param departureDate    The date of the departure
     * @param arrivalAirport   The 3 letter arrival airport code
     * @param currency         The 3 letter currency to convert to (if necessary)
     * @return A {@link Flux} that emits all {@link Flight} objects
     * that share the same lowest price
     */
    @GetMapping(BEST_PRICE_FLIGHTS)
    public Flux<Flight> findBestPrice(@RequestParam String departureAirport,
                                      @RequestParam String departureDate,
                                      @RequestParam String arrivalAirport,
                                      @RequestParam String currency) {
        return service
            // Forward request to service.
            .findBestPriceFlights(departureAirport,
                           LocalDate.parse(departureDate),
                           arrivalAirport,
                           currency);
    }

    /**
     * Find the exchange rate matching the {@code fromCurrency} and
     * {@code toCurrency} query parameters.
     *
     * @param fromCurrency The 3 letter currency code to exchange from
     * @param toCurrency   The 3 letter currency code to exchange to
     * @return A {@link Mono} emitting an {@link ExchangeRate} object
     * that includes the passed parameters along with a {@link Double}
     * exchange rate value
     */
    @GetMapping(RATE)
    public Mono<ExchangeRate> getRate(@RequestParam String fromCurrency,
                                      @RequestParam String toCurrency) {
        return service
                // Call the FlightService, which redirects the request
                // to the ExchangeRate microservice.
                .getRate(fromCurrency, toCurrency);
    }

    /**
     * Find the exchange rates matching the {@code toCurrency} query
     * parameter.
     *
     * @param toCurrency   The 3 letter currency code to exchange to
     * @return A {@link Mono} emitting an {@link ExchangeRate} object
     * that includes the passed parameters along with a {@link Double}
     * exchange rate value
     */
    @GetMapping(RATES)
    public Flux<ExchangeRate> getRates(@RequestParam String toCurrency) {
        return service
                // Call the FlightService, which redirects the request
                // to the ExchangeRate microservice.
                .getRates(toCurrency);
    }
}
