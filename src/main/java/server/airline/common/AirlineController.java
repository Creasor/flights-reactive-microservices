package server.airline.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

import reactor.core.publisher.Flux;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import static server.common.Constants.EndPoint.BEST_PRICE_FLIGHTS;
import static server.common.Constants.EndPoint.FLIGHTS;
import static server.common.Constants.EndPoint.FLIGHT_DATES;

/**
 * This Spring controller demonstrates how Spring MVC can be used to
 * handle HTTP GET requests via Java reactive programming.  These
 * requests are mapped to endpoint methods that forward to the {@link
 * AirlineService}, which asynchronously find all available flights,
 * find the best price for a flight request, and find departure dates
 * for a given pair of airports.  This controller is shared by all the
 * airline databases (e.g., American Airlines, Southwest Airlines,
 * etc.).
 *
 * In Spring's approach to building RESTful web services, HTTP
 * requests are handled by a controller (identified by the
 * {@code @RestController} annotation) that defines the endpoints (aka
 * routes) for each supported operation, i.e., {@code @GetMapping},
 * {@code @PostMapping}, {@code @PutMapping}, and {@code 
 * @DeleteMapping}, which correspond to the HTTP GET, POST, PUT, and
 * DELETE calls, respectively.
 *
 * Spring uses the {@code @GetMapping} annotation to map HTTP GET
 * requests onto methods in the {@linke AirlineController}.  GET
 * requests invoked from any HTTP web client (e.g., a web browser or
 * Android app) or command-line utility (e.g., Curl or Postman).
 *
 * The {@code @ResponseBody} annotation tells a controller that the
 * object returned is automatically serialized into JSON and passed
 * back into the HttpResponse object.  The {@code @CrossOrigin}
 * annotation marks the annotated method or type as permitting cross
 * origin requests, which is required for Eureka redirection.
 */
@RestController
@CrossOrigin("*") // Required for Eureka redirection
@ResponseBody
public class AirlineController {
    /**
     * This auto-wired field connects the {@link AirlineController} to
     * the {@link AirlineService} that stores airline information
     * persistently.
     */
    @Autowired
    AirlineService service;

    /**
     * Find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price(s) to the desired
     * {@code currency}.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport   The arrival airport
     * @return A {@link Flux} that emits matching {@link Flight}
     * objects in the desired currency
     */
    @GetMapping(FLIGHTS)
    public Flux<Flight> findFlights(@RequestParam String departureAirport,
                                    @RequestParam String departureDate,
                                    @RequestParam String arrivalAirport) {
        return service
            // Forward request to the service.
            .findFlights(departureAirport,
                         LocalDate.parse(departureDate),
                         arrivalAirport);
    }

    /**
     * Find the best priced flight(s) that match the given airports
     * and departure date.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport   The arrival airport
     * @return A {@link Flux} emitting {@link Flight} objects matching
     * the {@link FlightRequest} that share the lowest price in the
     * desired currency
     */
    @GetMapping(BEST_PRICE_FLIGHTS)
    Flux<Flight> findBestPrice(@RequestParam String departureAirport,
                               @RequestParam String departureDate,
                               @RequestParam String arrivalAirport) {
        return service
            // Forward request to service.
            .findBestPrice(departureAirport,
                           LocalDate.parse(departureDate),
                           arrivalAirport);
    }

    /**
     * Finds all departure dates that have at least one flight running
     * from the departure airport to the arrival airport.
     *
     * @param departureAirport departure airport
     * @param arrivalAirport   arrival airport
     * @return A {@link Flux} that emits all matching departure dates.
     */
    @GetMapping(FLIGHT_DATES)
    Flux<LocalDate> findDepartureDates(@RequestParam String departureAirport,
                                       @RequestParam String arrivalAirport) {
        return service
            // Forward request to service.
            .findDepartureDates(departureAirport, arrivalAirport);
    }
}