package server.airline.common;

import org.checkerframework.checker.units.qual.min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.math.MathFlux;
import server.common.CheapestPriceCollector;
import server.common.model.Flight;
import server.common.model.FlightRequest;

/**
 * This class defines implementation methods that are called by the
 * {@link AirlineController}.  These methods asynchronously find all
 * available flights, find the best price for a flight request, and
 * find departure dates for a given pair of airports.
 *
 * This class is annotated as a Spring {@code @Service}, which enables
 * the auto-detection and wiring of dependent implementation classes
 * via classpath scanning.
 */
@Service
public class AirlineService {
    /**
     * This auto-wired field connects the {@link AirlineService} to
     * the {@link AirlineRepository} that stores airline information
     * persistently.
     */
    @Autowired
    private AirlineRepository repository;

    /**
     * Find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price(s) to the desired
     * {@code currency}.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport   The arrival airport
     * @return A {@link Flux} that emits matching {@link Flight} objects
     */
    public Flux<Flight> findFlights(String departureAirport,
                                    LocalDate departureDate,
                                    String arrivalAirport) {
        return repository
            // Forward to the persistent repository.
            .findByDepartureAirportAndDepartureDateAndArrivalAirport
            (departureAirport,
             departureDate,
             arrivalAirport);
    }

    /**
     * Find the best priced flight(s) that match the given airports
     * and departure date.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport   The arrival airport
     * @return A {@link Flux} emitting {@link Flight} objects matching
     * the {@link FlightRequest} that share the lowest price in the
     * desired currency
     */
    public Flux<Flight> findBestPrice(String departureAirport,
                                      LocalDate departureDate,
                                      String arrivalAirport) {
        // Return the minimum price for the all matching flights.
        return findFlights(departureAirport,
                           departureDate,
                           arrivalAirport)

            // Convert a stream of Flights into a Flux that emits the
            // cheapest priced trips(s).
            .collect(CheapestPriceCollector.toFlux())

            // Convert the Mono into a Flux that emits the cheapest
            // priced trip(s).
            .flatMapMany(Function.identity());
    }

    /**
     * Finds all departure dates that have at least one flight running
     * from the departure airport to the arrival airport.
     *
     * @param departureAirport Departure airport
     * @param arrivalAirport   Arrival airport
     * @return A {@link Flux} that emits all matching departure dates
     */
    public Flux<LocalDate> findDepartureDates(String departureAirport,
                                              String arrivalAirport) {
        return repository
            // Forward to the persistent repository.
            .findDepartureDates(departureAirport, arrivalAirport);
    }
}
