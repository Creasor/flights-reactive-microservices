package server.exchange;

import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import server.common.model.ExchangeRate;

/**
 * A persistent repository that can be used to retrieve information
 * about {@link ExchangeRate} objects asynchronously.  The {@link
 * ExchangeRate} generic parameter is the domain type the repository
 * manages and the {@link Long} generic parameter is the id type of
 * the entity the repository manages.
 *
 * The {@code @Repository} annotation indicates that this class
 * provides the mechanism for storage, retrieval, search, update and
 * delete operation on {@link ExchangeRate} objects.
 */
public interface ExchangeRepository
        extends ReactiveCrudRepository<ExchangeRate, Long> {
    /**
     * Returns the {@link ExchangeRate} that matches the {@code
     * fromCurrency} and {@code toCurrency} query parameters.
     *
     * @param fromCurrency The 3 letter currency code to exchange from.
     * @param toCurrency   The 3 letter currency code to exchange to.
     * @return An {@link ExchangeRate} object containing the passed
     * parameters and a double exchange rate value
     */
    Mono<ExchangeRate> findByFromCurrencyAndToCurrency
        (@Param("fromCurrency") String fromCurrency,
         @Param("toCurrency") String toCurrency);

    /**
     * Returns a {@link Flux} that emits all rates for converting from
     * all know currencies to the provided {@code toCurrency}
     * parameter.
     *
     * @param toCurrency The 3 letter currency code to convert to
     * @return A {@link Flux} that emits a list of {@link
     * ExchangeRate} objects that contain the conversion rates from
     * all currencies to the provided {@code toCurrency} parameter
     */
    Flux<ExchangeRate> findByToCurrency(@Param("toCurrency") String toCurrency);
}
