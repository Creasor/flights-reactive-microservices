package server.exchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import server.common.model.ExchangeRate;

import static server.common.Constants.EndPoint.RATE;
import static server.common.Constants.EndPoint.RATES;

/**
 * This class defines an implementation method that is called by the
 * {@link ExchangeController} to find and return a {@link Mono} that
 * emits the current exchange rate for various currencies
 * asynchronously.
 * 
 * It is annotated as a Spring {@code @Service}, which enables the
 * auto-detection and wiring of dependent implementation classes via
 * classpath scanning.
 */
@Service
public class ExchangeService {
    /**
     * This auto-wired field connects the {@link ExchangeService} to
     * the {@link ExchangeRepository} that stores persistent {@link
     * ExchangeRate} objects.
     */
    @Autowired
    ExchangeRepository repository;

    /**
     * This method returns a Mono that emits the exchange rate between
     * the {@code fromCurrency} and {@code toCurrency} parameters
     * asynchronously.
     *
     * @param fromCurrency The 3 letter currency code to convert from
     * @param toCurrency   The 3 letter currency code to convert to
     * @return A {@link Mono} that emits the exchange rate between the
     * two passed currencies
     */
    @GetMapping(RATE)
    public Mono<ExchangeRate> getRate(@RequestParam String fromCurrency,
                                      @RequestParam String toCurrency) {
        // Return a Mono that emits the exchange rate.
        return repository
            // Forward to the persistent repository.
            .findByFromCurrencyAndToCurrency(fromCurrency, toCurrency);
    }

    /**
     * Returns a {@link Flux} that emits all rates for converting from
     * all known currencies to the provided {@code toCurrency}
     * parameter.
     *
     * @param toCurrency The 3 letter currency code to convert from
     * @return A {@link Flux{ that emits {@link ExchangeRate} objects
     * that contain the conversion rates from all known currencies to
     * the provided {@code toCurrency} parameter
     */
    @GetMapping(RATES)
    public Flux<ExchangeRate> getRates(String toCurrency) {
        // Return a Flux that emits a list of all exchange rates.
        return repository
            // Forward to the persistent repository.
            .findByToCurrency(toCurrency);
    }
}
